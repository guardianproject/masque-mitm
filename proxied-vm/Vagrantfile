
Vagrant.configure("2") do |config|
  config.vm.box = "debian/bullseye64"

  config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vm.hostname = "proxied-vm"
  config.vm.network "private_network",
                    ip: "10.3.3.3",
                    libvirt__network_name: "mitmproxy-transparent",
                    virtualbox__intnet: "mitmproxy-transparent",
                    auto_config: false # see ../interfaces.d/eth1

  config.vm.provider :libvirt do |libvirt|
    libvirt.cpus = 2
  end

  env = {
    "DEBIAN_FRONTEND" => "noninteractive",
    "LC_ALL" => "C.UTF-8",
  }

  config.vm.provision :file, source: "../if-up.d/del-default-route", destination: "del-default-route"
  config.vm.provision :file, source: "../apt.conf.d/99noninteractive", destination: "99noninteractive"
  config.vm.provision :file, source: "../interfaces.d/eth1", destination: "eth1"
  config.vm.provision :shell, env: env, inline: <<-SHELL
    set -ex

    update-locale LC_ALL=C.UTF-8

    install -m0644 99noninteractive /etc/apt/apt.conf.d/
    sed -i s,http:,https:,g /etc/apt/sources.list
    apt-get update
    apt-get dist-upgrade
    apt-get install curl

    install -m0755 del-default-route /etc/network/if-up.d/
    install -m0644 eth1 /etc/network/interfaces.d/
    rm 99noninteractive del-default-route eth1

    if ip route | grep default | grep eth0; then
        # eth0 is already up, and eth1 didn't exist yet, so manually trigger
        IFACE=eth0 /etc/network/if-up.d/del-default-route
    fi
    ifup eth1

    curl --silent http://mitm.it/cert/pem > /usr/local/share/ca-certificates/mitmproxy-ca-cert.crt
    update-ca-certificates
  SHELL
end
