
Vagrant.configure("2") do |config|
  config.vm.box = "debian/bullseye64"

  config.vm.synced_folder ".", "/vagrant", disabled: true

  config.vm.hostname = "proxy-router"
  config.vm.network "private_network",
                    ip: "10.3.3.254",
                    libvirt__network_name: "mitmproxy-transparent",
                    virtualbox__intnet: "mitmproxy-transparent"

  config.vm.provider :libvirt do |libvirt|
    libvirt.cpus = 2
  end

  env = {
    "DEBIAN_FRONTEND" => "noninteractive",
    "LC_ALL" => "C.UTF-8",
  }

  config.vm.provision :file, source: "../apt.conf.d/99noninteractive", destination: "99noninteractive"
  config.vm.provision :shell, env: env, inline: <<-SHELL
    set -ex

    update-locale LC_ALL=C.UTF-8

    install -m0644 99noninteractive /etc/apt/apt.conf.d/99noninteractive
    rm 99noninteractive
    sed -i s,http:,https:,g /etc/apt/sources.list
    apt-get update
    apt-get dist-upgrade
    apt-get install iptables-persistent mitmproxy
  SHELL

  # https://docs.mitmproxy.org/stable/howto-transparent-vms/
  config.vm.provision "shell", inline: <<-SHELL
    set -ex
    echo 'net.ipv4.ip_forward=1' > /etc/sysctl.d/01-ipv4-forward.conf
    sysctl --system

    for iptables in iptables ip6tables; do
        $iptables -F
        $iptables -t mangle -F
        $iptables -t nat -F

        # drop routed UDP, DNS happens locally so no routing
        $iptables -t mangle -A PREROUTING -i eth1 -p udp -j DROP

        # all routed TCP is forced into mitmproxy, SSH and DNS happen locally
        $iptables -t nat -A PREROUTING -i eth1 -p tcp -j REDIRECT --to-port 8080
    done
    iptables-save > /etc/iptables/rules.v4
    ip6tables-save > /etc/iptables/rules.v6
  SHELL

  # copied from https://github.com/abeluck/mitm-in-a-box/blob/master/mitm-server/mitmdump.service
  config.vm.provision :file, source: "mitmdump.service", destination: "mitmdump.service"
  config.vm.provision "shell", inline: <<-SHELL
    set -ex
    name=mitmdump.service
    install -m0644 $name /lib/systemd/system/
    rm $name
    systemctl daemon-reload
    systemctl enable $name
    systemctl start $name
  SHELL

end
