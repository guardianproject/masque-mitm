
This is a two VM setup for running _mitmproxy_.  One VM runs _mitmproxy_ in "[transparent](https://docs.mitmproxy.org/stable/howto-transparent-vms/)" mode and acts as the router.  The other is then routed through that router, which forces all its traffic through _mitmproxy_.  It should run in VirtualBox and _libvirt_ using Vagrant.

## Setup

The _proxy-router_ needs to be running before the _proxied_vm_ because the _proxied_vm_ configures itself to use the _proxy-router_ as its sole internet gateway.

```console
$ git clone https://gitlab.com/guardianproject/masque-mitm.git
$ cd masque-mitm/proxy-router
$ vagrant up
$ vagrant ssh -c "tail -f mitmdump.log"
```

Then for the MITM'ed session:
```console
$ cd ../proxied-vm
$ vagrant up
$ vagrant ssh # run the stuff you want MITM'ed here
```

### Optionally running the interactive mitmproxy console

If you want to use the interactive console instead of the raw dump log, do this:

```console
$ cd masque-mitm/proxy-router
$ vagrant ssh -c "sudo service mitmdump stop"
$ vagrant ssh -c "sudo mitmproxy --mode transparent --verbose"
```


## Implementation

This was done using only Vagrant shell provisioning to keep things simple.  This should hopefully also make it possible to run this on Docker and Podman.
